<?php include 'header.php';?>
<!--SEARCH FORM -->
<form action="" method="POST">
    <div class="input">
        <input type="text" id="search" name="search" class="box" placeholder="Search student" autocomplete="off">
        <button class="btn">Search</button>
        <small id="searchHelp" class="form-text text-muted">search for 'name','email','course'</small>
    </div>
</form>
<!--SELECT ACTION FORM-->
<div class="search">
    <div class="mb-3">
        <select class="form-select form-select-lg mb-3"  aria-label=".form-select-lg example" onchange="location=this.value;">
        <option selected>Select Action</option>
        <option value="index.php">Add Student</option>
        </select>
    </div>
</div>

<!--PAGINATION LOGIC-->
    <?php
        include 'config.php';
        try{
                $limit=5;
                if(isset($_GET['page'])){
                    $page=$_GET['page'];
                }else{
                        $page=1;
                    }
                    $offset=($page-1)*$limit;
                    //Search Logic
                    if(isset($_POST['search'])){
                        $search_key=$_POST['search'];
                        $sql = "SELECT s.*,(SELECT GROUP_CONCAT(interest)
                        FROM interests h
                        WHERE h.student_id= s.id) AS interests
                        FROM student s where name LIKE '%$search_key%' or class_name LIKE '%$search_key' or email LIKE '%$search_key%' ";
                    }else{
                    $sql = "SELECT s.*,(SELECT GROUP_CONCAT(interest)
                            FROM interests h
                            WHERE h.student_id= s.id) AS interests
                            FROM student s ORDER BY id desc LIMIT {$offset},{$limit}";
                    }
                        $result=$obj->getStudent($sql);
                    if(mysqli_num_rows($result)){
    ?>

   <table id="myTable">
        <thead>
            <th><input type="checkbox"></th>
            <th>Id</th>
            <th>Name</th>
            <th>Course</th>
            <th>Email</th>
            <th>interests</th>
            <th>Status</th>
            <th>Resume</th>
            <th>Actions</th>
            </td>
        </thead>
        <tbody>
            <form method="post" action="">
        <?php
            while($row = mysqli_fetch_assoc($result)){
        ?>
            <tr>
            <td>
                <input type="checkbox" name="value[]" value="<?php echo $row["id"]; ?>" class="checkItem">
            </td>
                <td><?php echo $row['id']; ?></td>
                <td><?php echo $row['name']; ?></td>
                <td><?php echo $row['class_name']; ?></td>
                <td><?php echo $row['email']; ?></td>
                <td><?php echo $row['interests']; ?></td>
                <td>
                    <?php
                    if($row['status']==0){
                    ?>
                    <button class="btn btn-success">Active</button>
                    <?php
                    }
                    else{
                    ?>
                        <button class="btn btn-danger">Inactive</button>
                    <?php
                         }
                    ?>
                   </td>
                <td><?php echo $row['resume']; ?></td>
                <td>
                    <a id="edit" href="student_edit.php?id=<?php echo $row["id"]; ?>"><i class="fa fa-edit"></i></a>
                    <a id="delete" onclick="javascript:confirmationDelete($(this));return false;" href="student_delete.php?id=<?php echo $row["id"]; ?>"><i class="fa fa-trash"></i></a>
                </td>
            </tr>

            <?php
                    }
            ?>
        <div class="row">
            <input type="submit" name="delete" value="Delete" class="delete">
            </form>
        </div>

       <?php
        try{
            if(isset($_POST['delete'])){
                $deletedvalue = $_POST['value'];
                $numberofcheckbox=is_countable($deletedvalue);
                // print_r($numberofcheckbox);
                // exit;
                $i=0;
                while($i<$numberofcheckbox){
                    $keytodelete=$_POST['value'][$i];
                    $output="DELETE from student where id='$keytodelete'";
                    $obj->deleteStudent($output);
                    $i++;

                }
            }
            //     foreach($_POST['id'] as $deleteid){

            //    $deleteUser = "DELETE from users WHERE id=".$deleteid;
            //    $obj->deleteStudent($deleteUser);

             //if(isset($_POST['multipledelete'])){
                // if(isset($_POST['id'])){
                //     foreach($_POST['id'] as $id){
                //             $query = "DELETE * FROM student WHERE id='$id'";
                //             $obj->deleteStudent($query);
                //     }

                // }
             //}
                // $sql ="SELECT *FROM students";
                // $result=$obj->getStudent($sql);

        }catch(error $error){
            echo"something went wrong".$error->getMessage();
        }
        ?>
        </tbody>
    </table>
    <?php
            }
            }catch(error $e){
                echo "Try again".$e->getMessage();
                exit;
            }

            $sql1="SELECT * FROM student";
            $result1=$obj->getStudent($sql1);
            if(mysqli_num_rows($result1)>0){
                $total_records=mysqli_num_rows($result1);
                // $limit=3;
                $total_pages=ceil($total_records/$limit);

                echo '<ul id="first" class="pagination">';
                for($i=1;$i<=$total_pages;$i++){
                        echo '<li id="page"><a id="second" href="student_list.php?page='.$i.'">'.$i.'</a></li>';
                }
                echo '</ul>';
            }

    ?>
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
 <!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<script>
    function confirmationDelete(anchor){
            var conf = confirm('Are you want to delete this record?');
            if(conf)
            window.location=anchor.attr("href");
        }

    $(document).ready(function(){
        document.getElementById('second').style.padding="10px";
        document.getElementById('second').style.margin="10px 10px 10px 100px";
        document.getElementById('first').style.margin="50px 800px 10px 500px";

    });
</script>
<?php include 'footer.php';?>