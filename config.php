<?php
session_start();
class config {
     public $host="localhost";
     public $username="root";
     public $password="";
     public $dbname="student";
     public $con;

    public function __construct(){
        $this->con=new mysqli($this->host,$this->username,$this->password,$this->dbname);
        if(mysqli_connect_errno()){
            echo "Database connection Failed";

        }
    }

    public function insert($data) {
        $this->con->query($data);
        if ($this->con->query($data)) {
            $last_id = $this->con->insert_id;
            return $last_id;
        }
    }

    public function auditInsert($data){

        $this->con->query($data);
    }

    public function insertInterest($data){

       $result= $this->con->query($data);
    }

    public function getStudent($data){
       $result= $this->con->query($data);
        return $result;

    }

    public function getInterest($data){
       $result= $this->con->query($data);
        return $result;

    }

    public function deleteStudent($data){
        $result= $this->con->query($data);

    }

    public function update($data){
        $this->con->query($data);

    }

    public function url($url){
        header("Location:".$url);
    }
}

 $obj=new config;

?>