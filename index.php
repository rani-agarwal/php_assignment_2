<?php
include 'header.php';
    $token=md5(uniqid('rand'));
    $_SESSION['csrf_token']=$token;
?>
    <div class="card" style="width: 50rem;">
        <div class="card-body">
            <h5 class="card-title">STUDENT DETAILS</h5>
            <form action="student_add.php" method="POST" enctype="multipart/form-data" id="myform">

                <div class="form-group">
                    <label for="exampleInputName"><b>Name</b></label>
                    <input type="hidden" class="form-control" name="token"  value="<?php echo $token; ?>" id="exampleInputName"
                        placeholder="Enter Your name" required>
                    <input type="text" class="form-control" name="name" id="exampleInputName"
                        placeholder="Enter your name" required autocomplete="off">
                </div>

                <div class="mb-3">
                    <label for="exampleInputType" class="form-label"><b>Student Class</b></label>
                    <select class="form-select form-select-lg mb-3" name="class_name"
                        aria-label=".form-select-lg example" required>
                        <option selected>--Select--</option>
                        <option value="c">C</option>
                        <option value="c++">C++</option>
                        <option value="python">Python</option>
                        <option value="php">PHP</option>
                        <option value="java">JAVA</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1"><b>E-mail</b></label>
                    <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelp"
                        placeholder="Enter your email" required autocomplete="off">
                </div>

                <div>
                    <label for="interest"><b>Interests</b></label><br>
                    <label><input type="checkbox" name="interest[]" value="dancing">Dancing</label>
                    <label><input type="checkbox" name="interest[]" value="singing">Singing</label>
                    <label><input type="checkbox" name="interest[]" value="art">Art</label>
                    <label><input type="checkbox" name="interest[]" value="other">other</label>
                </div>

                <div class="form-group">
                    <label for="exampleInputFile"><b>Upload Resume</b></label>
                    <input type="file" class="form-control" name="resume" id="exampleInputFile" required>
                </div>

                <div class="form-group">
                        <label><b>Status</b></label><br>
                        <input type="radio"  name="status" value="active" required>Active
                        <input type="radio" name="status" value="inactive" class="ml-5">Inactive
                </div>

                <div>
                    <input type="submit" name="submit" value="Submit" class="btn btn-primary">
                    <button type="reset" class="btn btn-primary">Reset</button>
                </div>

            </form>
        </div>
    </div>
<?php include 'footer.php';?>