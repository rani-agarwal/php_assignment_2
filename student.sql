-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 01, 2021 at 01:48 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `student`
--

-- --------------------------------------------------------

--
-- Table structure for table `audit_log`
--

CREATE TABLE `audit_log` (
  `id` int(11) NOT NULL,
  `action` varchar(50) NOT NULL,
  `student_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `audit_log`
--

INSERT INTO `audit_log` (`id`, `action`, `student_id`, `created_at`) VALUES
(1, 'created', 83, '2021-12-01 10:10:42'),
(2, 'deleted', 82, '2021-12-01 10:12:06'),
(3, 'updated', 0, '2021-12-01 10:12:38'),
(4, 'updated', 0, '2021-12-01 10:15:23'),
(5, 'created', 85, '2021-12-01 10:17:11'),
(6, 'created', 87, '2021-12-01 10:17:53'),
(7, 'updated', 0, '2021-12-01 10:18:27'),
(8, 'created', 89, '2021-12-01 10:19:11'),
(9, 'created', 91, '2021-12-01 10:26:56'),
(10, 'updated', 0, '2021-12-01 10:41:27'),
(11, 'updated', 0, '2021-12-01 10:48:30'),
(12, 'updated', 0, '2021-12-01 10:48:49'),
(13, 'updated', 0, '2021-12-01 11:02:51'),
(14, 'deleted', 91, '2021-12-01 11:09:20'),
(15, 'created', 93, '2021-12-01 11:11:38'),
(16, 'created', 95, '2021-12-01 11:12:41'),
(17, 'created', 97, '2021-12-01 11:16:56'),
(18, 'created', 99, '2021-12-01 11:20:10'),
(19, 'deleted', 98, '2021-12-01 11:56:56'),
(20, 'created', 102, '2021-12-01 11:58:13'),
(21, 'deleted', 101, '2021-12-01 11:58:25'),
(22, 'created', 104, '2021-12-01 11:58:57'),
(23, 'deleted', 103, '2021-12-01 11:59:25'),
(24, 'created', 106, '2021-12-01 12:00:14'),
(25, 'deleted', 105, '2021-12-01 12:00:22'),
(26, 'created', 108, '2021-12-01 12:26:55'),
(27, 'deleted', 107, '2021-12-01 12:27:00');

-- --------------------------------------------------------

--
-- Table structure for table `interests`
--

CREATE TABLE `interests` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `interest` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `interests`
--

INSERT INTO `interests` (`id`, `student_id`, `interest`) VALUES
(1, 83, 'dancing'),
(2, 83, 'singing'),
(3, 85, 'dancing'),
(4, 85, 'singing'),
(5, 87, 'art'),
(6, 89, 'art'),
(7, 91, 'singing'),
(8, 93, 'singing'),
(9, 95, 'dancing'),
(10, 97, 'singing'),
(11, 99, 'singing'),
(12, 100, 'arts,dancing'),
(13, 102, 'art'),
(14, 104, 'singing'),
(15, 106, 'dancing'),
(16, 108, 'singing');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `class_name` varchar(30) NOT NULL,
  `email` varchar(80) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `resume` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `name`, `password`, `class_name`, `email`, `status`, `resume`, `created_at`) VALUES
(83, 'Rani Agarwal', 'OtfM1I', 'php', 'raniag681@gmail.com', 0, 'PHP_Test_2.pdf', '2021-12-01 10:10:42'),
(99, 'Sakshi', 'kOyjsn', 'python', 'sakshi@gmail.com', 1, 'softskill.pdf', '2021-12-01 11:20:09'),
(100, 'Priyesh', 'lPyjeq', 'Java', 'priyesh@hmail.com', 0, 'softskill.pdf', '2021-12-01 12:08:05'),
(102, 'xyz', 'Uz1eLR', 'c++', 'xyz@gmail.com', 1, 'Javascript.pdf', '2021-12-01 11:58:12'),
(104, 'abc', 'RMOk0s', 'c', 'abc@gmail.com', 1, 'softskill.pdf', '2021-12-01 11:58:57'),
(108, 'Ritul', 'nmueLK', 'c', 'ritul@gmail.com', 0, 'softskill.pdf', '2021-12-01 12:26:55');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `audit_log`
--
ALTER TABLE `audit_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `interests`
--
ALTER TABLE `interests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`student_id`) USING BTREE;

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `audit_log`
--
ALTER TABLE `audit_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `interests`
--
ALTER TABLE `interests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
